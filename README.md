# MORMON

Meu ORM ou Nada.

Biblioteca de Mapeamento Objeto-Relacional.
A proposta da biblioteca é simples: Configuração zero. Sem anotações ou declarações.

## Como utilizar?
Arquivo Comment.php
```php
namespace App;

use Mormon\Mormon;

class Comment extends Mormon {
    public function getPost(){
        return $this->getOne(Post::class);
    }
}
```
Arquivo Post.php
```php
namespace App;

use Mormon\Mormon;

class Post extends Mormon {
    public function getComments(){
        return $this->getMany(Comment::class)
    }
}
```
Arquivo index.php
```php
require 'vendor/autoload.php';

use Mormon\Mormon;
use App\Post;
use App\Comment;

// Configurações necessárias para conexão com o banco.
$config = [
    "dsn" => "mysql:host=localhost;dbname=test",
    "user" => "root",
    "password" => "12345",
    "options" = []
];

// Inicialização da conexão
Mormon::init($config);

// ... Criando um novo registro no BD
$post = new Post();
$post->set('timestamp', date('Y-m-d H:i:s'))
     ->set('title', 'Meu primeiro post')
     ->set('text', 'Hora de gravar!')
     ->create();

// ... Atualizando os dados no BD
$post->set('text', 'Hora de atualizar!')->update();

// ... Obtendo informações de relacionamentos.
$post = new Post($id);
$comments = $post->getComments();

foreach ($comments as $comment){
    // Faça alguma coisa com os objetos.
}
```
E nada mais.

## Quais as convenções utilizadas?
* Espera-se que cada tabela deve ter uma chave primária (PK) simples;
* Adota-se como chave primária o campo `id`, porém pode ser modificado configurando-se o atributo `public static $pk = 'id'` no escopo da entidade implementada.
* Para o nome da tabela, é a realizada a conversão CamelCase &rarr; snake_case, porém é possível usar um nome diferente configurando o atribuito `public static $table = 'tabela'`;
* Para os relacionamentos, as chaves estrangeiras (FK) adotam o nome do campo como tabela_PK. Consulte a documentação para configurações personalizadas de chaves estrangeiras.
