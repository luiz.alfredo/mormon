<?php

use Mormon\Mormon;

class Post extends Mormon {
    public function getPostTags(){
        return $this->getMany(PostTag::class);
    }
}