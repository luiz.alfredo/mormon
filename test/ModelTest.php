<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Mormon\Mormon;

require "Post.php";
require "Tag.php";
require "PostTag.php";


final class ModelTest extends TestCase
{

    public function setUp() : void {
        Mormon::init([
            'dsn'      => "mysql:host=127.0.0.1;dbname=test_schema",
            'user'     => "root",
            'password' => "12345",
            'options'  => [
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
            ]
        ]);
    }

    public function testCRUD(): void
    {
        $post = new Post();
        $post->set('datetime', date('Y-m-d H:i:s'));
        $post->set('title', 'New Post');
        $post->set('text', 'Title of a new Post.');

        $this->assertTrue(
            $post->create()
        );
        $post->set('title', 'New Title');
        $this->assertTrue(
            $post->update()
        );
        $this->assertEquals(
            $post->get('title'),
            'New Title'
        );

    }

    public function testRelationships() : void {

        $tags = [];
        $tags[] = Tag::load(['name' => 'People']);
        $tags[] = Tag::load(['name' => 'Business']);
        $tags[] = Tag::load(['name' => 'Hobbies']);

        $post = new Post();
        $post->set('datetime', date('Y-m-d H:i:s'));
        $post->set('title', 'My Post with tags');
        $post->set('text', 'Testing tags');
        $post->create();

        foreach ($tags as $tag){
            
            $tag->create();

            PostTag::load([
                'tag_id' => $tag->get('id'),
                'post_id' => $post->get('id')
            ])->create();
        }

        $newTags = $post->getPostTags();

        $this->assertCount(3, $newTags);

    }

    public function tearDown() : void {
        Mormon::$pdo = null;
    }
}