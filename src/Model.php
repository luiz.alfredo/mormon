<?php

namespace Mormon;

use PDO, Exception;

/**
 * Classe representativa da camada de Modelo do sistema. Um modelo representa uma
 * entidade no banco de dados e seus relacionamentos.
 */
class Model {

	/**
	 * Conexão com o Banco de Dados.
	 * @param PDO
	 */
	protected static $pdo = null;

	/**
	 * Array para recuperacao de relacionamentos
	 * com chaves estrangeiras.
	 * 
	 * @var Model[]
	 */
	protected $relationships = [];

	/**
	 * Tabela referente ao modelo.
	 * Caso não seja fornecido o nome da tabela, convenciona-se a transformação
	 * do nome da classe de CamelCase para snake_case.
	 *
	 * @var string
	 */
	public static $table = "";

	/**
	 * Nome do campo da Chave Primária.
	 * Caso não seja fornecido, convenciona-se o uso do campo `id` como
	 * chave primária.
	 *
	 * @var string
	 */
	public static $pk = "id";
	
	/**
	 * Atributos do modelo
	 *
	 * @var array
	 */
	private $attrs = [];
	
	/**
	 * Método construtor do modelo
	 *
	 * @param int $id Chave primária da entidade.
	 */
	public function __construct($id = null){		
		if ($id)
			$this->read($id);
	}

	/**
	 * Fornece o nome da tabela baseado no nome da própria classe (CamelCase para snake_case)
	 * ou como declarado no atributo $table da classe.
	 * @return string Nome da tabela correspondente no banco de dados.
	 */
	public static function getTable(){
        if (!static::$table) {
			$a = explode("\\", static::class);
			$class = array_pop($a);
            return strtolower(preg_replace("/([a-z])([A-Z])/", "$1_$2", $class));
        } else {
			return static::$table;
		}
	}

	/**
	 * Retorna o objeto PDO usado pelas entidades.
	 *
	 * @return PDO Objeto de conexão com o Banco de dados.
	 */
	public static function getPDO() : PDO {
		return static::$pdo;
	}

	/**
	 * Configura um objeto de conexão PDO pré-existente.
	 *
	 * @param PDO $pdo Objeto de conexão com o Banco de Dados
	 * @return void
	 */
	public static function setPDO(PDO $pdo) {
		static::$pdo = $pdo;
	}

	/**
	 * Inicializa o Gerenciador do Banco de Dados.
	 * É necessário passar um objeto com os seguintes atributos: dsn, user, password e options.
	 * O atributo options é usado na instanciação do objeto PDO.
	 * 
	 * @param object Config Configurações da conexão
	 * @return bool Verdadeiro em caso de sucesso, ou falso, em caso de falha
	 */
	public static function init($config) : bool {
		static::$pdo = new PDO(
			$config['dsn'],
			$config['user'],
			$config['password'],
			$config['options']			
		);
		return !static::$pdo->errorCode();
	}

	/**
	 * Relaciona a entidade com um único correspondente.
	 * @param $class string Nome da classe da entidade correspondida
	 * @param $name string Nome do Relacionamento. Por convenção, utiliza-se o nome da tabela destino
	 * @param $fk string Nome da chave estrangeira
	 * @return Model A entidade instanciada
	 */
	protected function getOne($class, $name = false, $fk = false) : Model {

		$name = $name? $name : $class::getTable();
		$fk = $fk? $fk : "{$name}_{$class::$pk}";

        if (!isset($this->relationships[$name])) {
            $this->relationships[$name] = new $class($this->get($fk));
        }

		return $this->relationships[$name];

	}

	/**
	 * Relaciona a entidade com vários correspondentes.
	 * @param $class string Nome da classe da entidade correspondida
	 * @param $name string Nome do Relacionamento
	 * @param $fk string Nome da chave estrangeira
	 * @param $filter string Parâmetro para filtragem do relacionamento
	 * @return Model[] As entidades instanciadas
	 */
	protected function getMany($class, $name = false, $fk = false, $filter = "TRUE", $params = []) : array {	

		$name = $name? $name : $class::getTable();
		$fk = $fk? $fk : static::getTable() . '_' . static::$pk;
		$id = $this->get(static::$pk);

		$params[$fk] = $id;

		if (!isset($this->relationships[$name]))
			$this->relationships[$name] = $class::all("$fk = :$fk AND $filter", $params);

		return $this->relationships[$name];
	}

	/**
	 * Cria um novo registro no banco de dados
	 *
	 * @return boolean Verdadeiro, em caso de sucesso, ou falso, caso contrário
	 */
	public function create() : bool {
		
		$table = static::getTable();
		
		$attrs = array_filter($this->attrs, function($k){
			return $k != static::$pk;
		}, ARRAY_FILTER_USE_KEY);

		$fields = implode(", ", array_keys($attrs));

		$values = implode(", ", array_map(function($k){
			return ":$k";
		}, array_keys($attrs)));
		
		$sql = "INSERT INTO `{$table}`($fields) VALUES ($values)";
		
		$statement = self::$pdo->prepare($sql);
		return $statement->execute($attrs) && $this->read(self::$pdo->lastInsertId());
	}
	
	/**
	 * Realiza a leitura dos dados no banco a partir do id fornecido ou configurado.
	 *
	 * @return boolean Verdadeiro, em caso de sucesso, ou falso, caso contrário
	 */
	public function read($id = false) : bool {

		$pk = static::$pk;
		$id = $id? $id : $this->get(static::$pk, $id);

        if ($id) {
            $table = static::getTable();
            $sql = "SELECT * FROM {$table} WHERE $pk = :pk";
            $statement = self::$pdo->prepare($sql);
			$statement->execute(['pk' => $id]);
            
			$this->attrs = $statement->fetch(PDO::FETCH_ASSOC);
			
        }

		return ($this->attrs && true);
	}

	/**
	 * Atualiza os dados a entidade no banco de dados.
	 *
	 * @return boolean Verdadeiro em caso de sucesso, ou falso, caso contrário.
	 */	
	public function update() : bool {
		
		$pk = static::$pk;
		
        if ($id = $this->get($pk)) {

			$fields = array_filter($this->attrs, function ($k) use ($pk){
				return $k != $pk;
			}, ARRAY_FILTER_USE_KEY);

			$updates = array_map(function($key){
				return "$key = :$key";
			}, array_keys($fields));

			$updates = implode (', ', $updates);

			$table = static::getTable();
			$sql = "UPDATE {$table} SET $updates WHERE $pk = :id";

			$fields[$pk] = $id;

			$statement = self::$pdo->prepare($sql);
			$result = $statement->execute($fields);
								
			return ($result && $this->read());


		} else {
			return $this->create();
		}
	}
	
	/**
	 * Deleta o registro no banco de dados.
	 *
	 * @return boolean Verdadeiro, em caso de sucesso, ou falso, caso contrário
	 */
	public function delete() : bool {

        if ($this->read()) {
            $pk = static::$pk;

            $table = static::getTable();
            $sql = "DELETE FROM {$table} WHERE $pk = :pk";
            $params = ['pk' => $this->get($pk)];

            $statement = self::$pdo->prepare($sql);
            return $statement->execute($params);

        } else {
			return true;
		}		
	}
	
	/**
	 * Recupera um atributo da entidade
	 *
	 * @param string $field Nome do atributo
	 * @param string $default Valor padrão, caso não esteja configurado
	 * @return string Valor do atributo
	 */
	public function get($field, $default = null) {
		if (isset($this->attrs[$field]) && !is_null($this->attrs[$field]) && $this->attrs[$field] != ''){
			return $this->attrs[$field];
		} else {
			return $default;
		}
	}	

	/**
	 * Seta um atributo da entidade
	 *
	 * @param string|array $field Nome do atributo
	 * @param string $value Valor Valor do atributo
	 * @return Model A própria Entidade, para encadeamento de métodos
	 */	
	public function set($field, $value = null) : Model {
		if (is_array($field)){
			foreach ($field as $k => $v) $this->set($k, $v);
		} else {
            $this->attrs[$field] = $value;
        }
		return $this;
	}
		
	/**
	 * Cria uma nova entidade genérica com os dados informados.
	 *
	 * @param array $data Dados no formato [chave => valor]
	 * @return Model O novo modelo
	 */
	public static function load($data) : Model {
		$entity = new static;
		$entity->set($data);	
		return $entity;
	}
	
	/**
	 * Retorna um array com todas as entidades filtradas
	 *
	 * @param string $filter Filtro da cláusula WHERE
	 * @param array $params Parâmetros para filtragem
	 * @return Model[] Array de entidades
	 */
	public static function all($filter = false, $params = []) : array {

		$pk = static::$pk;
		$table = static::getTable();

		$sql = "SELECT * FROM $table WHERE ". ($filter? $filter : 1);
		$statement = static::$pdo->prepare($sql);
		$statement->execute($params);

		$entities = [];

		while ($data = $statement->fetch(PDO::FETCH_ASSOC))
			$entities[] = static::load($data);

		return $entities;
		
	}
	
}